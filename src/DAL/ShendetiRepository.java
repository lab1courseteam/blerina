/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Shendeti;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author elina
 */
public class ShendetiRepository extends EntMngClass implements ShendetiInterface {    
public void create(Shendeti sh) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(sh);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void edit(Shendeti sh) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(sh);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void delete(Shendeti sh) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(sh);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public List<Shendeti> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Shendeti.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public Shendeti findByID(Integer ID) {
       Query query = em.createQuery("SELECT e FROM Shendeti e WHERE e.ID = :id");
        query.setParameter("id", ID);
        return (Shendeti)query.getSingleResult();
    }
    }


