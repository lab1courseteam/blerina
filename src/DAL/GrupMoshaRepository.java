package DAL;

import BLL.GrupMosha;
import java.util.List;
import javax.persistence.Query;

public class GrupMoshaRepository extends EntMngClass implements GrupMoshaInterface {

    public void create(GrupMosha gm) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(gm);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void edit(GrupMosha gm) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(gm);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void delete(GrupMosha gm) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(gm);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public List<GrupMosha> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("GrupMosha.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    public GrupMosha findByID(Integer id) {
        Query query = em.createQuery("SELECT e FROM GrupMosha e WHERE e.ID = :id");
        query.setParameter("id", id);
        return (GrupMosha)query.getSingleResult();
    }

}
