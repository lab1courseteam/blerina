/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Shendeti;
import java.util.List;

public interface ShendetiInterface {
     void create(Shendeti sh) throws CrudFormException;
    void edit(Shendeti sh) throws CrudFormException;
    void delete(Shendeti sh) throws CrudFormException;
    List<Shendeti> findAll() throws CrudFormException;
    Shendeti findByID(Integer ID);

}
